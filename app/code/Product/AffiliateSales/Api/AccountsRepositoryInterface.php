<?php

namespace Product\AffiliateSales\Api;

interface AccountsRepositoryInterface
{
  /**
  * @return \Product\AffiliateSales\Api\Data\AccountsInterface[]
  */
  public function getList();
}
