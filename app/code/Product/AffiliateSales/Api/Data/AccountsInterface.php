<?php
namespace Product\AffiliateSales\Api\Data;

interface AccountsInterface
{
  /** @return string */
  public function getName();

  /** @return string|null */
  public function getDescription();


}
