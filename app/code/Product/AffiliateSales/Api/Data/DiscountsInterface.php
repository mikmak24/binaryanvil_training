<?php
namespace Product\AffiliateSales\Api\Data;

interface DiscountsInterface
{
    /** @return string */
    public function getName();

    /** @return string|null */
    public function getDescription();


}
