<?php
namespace Product\AffiliateSales\Api\Data;

interface ProductsInterface
{
    /** @return string */
    public function getName();

    /** @return string|null */
    public function getDescription();


}
