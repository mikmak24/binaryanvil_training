<?php

namespace Product\AffiliateSales\Api;

interface DiscountsRepositoryInterface
{
    /**
     * @return \Product\AffiliateSales\Api\Data\DiscountsInterface[]
     */
    public function getList();
}
