<?php

namespace Product\AffiliateSales\Api;

interface ProductsRepositoryInterface
{
    /**
     * @return \Product\AffiliateSales\Api\Data\ProductsInterface[]
     */
    public function getList();
}
