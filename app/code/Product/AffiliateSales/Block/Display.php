<?php
namespace Product\AffiliateSales\Block;

use Product\AffiliateSales\Model\AccountsFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http;
use Product\AffiliateSales\Model\ProductsRepository;
use Product\AffiliateSales\Model\DiscountsRepository;

class Display extends \Magento\Framework\View\Element\Template
{

    private $_products;
    protected $_accounts;
    protected $customerSession;
    protected $_orderCollectionFactory;
    protected $request;
    protected $repo;
    protected $commisionrepo;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
     \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
     \Product\AffiliateSales\Model\Products $product,
     \Magento\Framework\App\ResourceConnection $resource,
     AccountsFactory $accounts,
     ProductsRepository $repo,
     Session $customerSession,
     Http $request,
    DiscountsRepository $commisionrepo,
     array $data = []
    ){
        $this->_accounts = $accounts;
        $this->_products = $product;
        $this->_resource = $resource;
        $this->repo = $repo;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->request = $request;
        $this->commisionrepo = $commisionrepo;
		parent::__construct($context, $data);
        $this->customerSession = $customerSession;
	}

	public function getAffiliateProducts()
    {
        $collection = $this->_products->getCollection();
        return $collection;
    }

    public function getTrackingcode()
    {
        $customerEmail = $this->getCustomerSession();
        $collection = $this->_accounts->create();
        $multiselectupdate = $collection->getCollection()
            ->addFieldToFilter('email_address', $customerEmail)->getData();

        $trackingcode = '';

        foreach ($multiselectupdate as $multi){
            if ($multi['email_address']){
                $trackingcode = $multi['trackingcode'];
            }
        }

        return $trackingcode;

    }

    public function getCustomerSession()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }

    public function getTransactionOrders()
    {
        $collection = $this->_orderCollectionFactory->create()->addAttributeToSelect('*');
        return $collection;
    }

    public function getTrackingUrl()
    {
        return $this->request->getParam('trackingcode');
    }

    public function setAffiliateSku($trackingcode, $productSku, $selectedValue)
    {
        $this->repo->setAffiliateSku($trackingcode, $productSku, $selectedValue);
    }

    public function getAffiliatedDiscount()
    {
        return $this->commisionrepo->getAffiliatedDiscount();
    }
}
