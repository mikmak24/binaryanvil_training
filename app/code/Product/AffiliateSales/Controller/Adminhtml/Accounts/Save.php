<?php
namespace Product\AffiliateSales\Controller\Adminhtml\Accounts;

use Product\AffiliateSales\Model\AccountsFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    private $accountsFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        AccountsFactory $accountsFactory
    ) {
        $this->accountsFactory = $accountsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->accountsFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('affiliates/index/index');
    }
}
