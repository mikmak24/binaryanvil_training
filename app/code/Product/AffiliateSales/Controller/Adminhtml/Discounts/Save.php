<?php
namespace Product\AffiliateSales\Controller\Adminhtml\Discounts;

use Product\AffiliateSales\Model\DiscountsFactory;
use Magento\Backend\App\Action;


class Save extends Action
{
    private $discountsFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        DiscountsFactory $discountsFactory
    ) {
        $this->discountsFactory = $discountsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->discountsFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('discounts/index/index');
    }
}
