<?php
namespace Product\AffiliateSales\Controller\Adminhtml\Products;

use Product\AffiliateSales\Model\ProductsFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    private $productsFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        ProductsFactory $productsFactory
    ) {
        $this->productsFactory = $productsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->productsFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('products/index/index');
    }
}
