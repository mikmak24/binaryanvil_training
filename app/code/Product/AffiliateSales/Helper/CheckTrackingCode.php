<?php

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ResourceConnection;

class CheckTrackingCode extends AbstractHelper
{

    protected $productRepository, $logger, $resource;

    public function __construct
    (
        Logger $logger,
        ResourceConnection $resource
    )
    {

        $this->logger = $logger;
        $this->resource = $resource;
    }
}