<?php

namespace Product\AffiliateSales\Model;

use Product\AffiliateSales\Api\AccountsRepositoryInterface;
use Product\AffiliateSales\Model\ResourceModel\Accounts\CollectionFactory;
use Magento\Framework\App\ResourceConnection;

class AccountsRepository implements AccountsRepositoryInterface
{
    private $collectionFactory;
    private $resource;

    public function __construct(CollectionFactory $collectionFactory, ResourceConnection $resource)
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
    }

    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function setAffiliates($firstname, $lastname, $email_address, $trackingcode)
    {
        $model = $this->collectionFactory->create();

        $model->getConnection()->insert('affiliate_accounts', ['firstname' => $firstname,'lastname' => $lastname,
          'email_address' => $email_address, 'trackingcode' => $trackingcode]);
        $model->save();
    }

    public function getTrackingCode()
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $check = "SELECT trackingcode FROM $tableName";
        return $list = $connection->fetchAll($check);
    }

    public function setCommisionRate($trackingcode, $commisionrate)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $initialrate = "SELECT commisionrate FROM $tableName WHERE trackingcode = '$trackingcode'";
        $rate = $connection->fetchOne($initialrate);

        if(is_null($initialrate))
        {
            $setRate = "INSERT INTO $tableName(commisionrate) VALUES('$commisionrate')";
            $connection->query($setRate);
        }
        else
        {
            $num1 = (int)$initialrate;
            $num2 = (int)$commisionrate;
            $sum = ($num1 + $num2);
            $newRate = (string)$sum;
            $setRate = "UPDATE $tableName(commisionrate) VALUES('$newRate')";
            $connection->query($setRate);
        }
    }


}
