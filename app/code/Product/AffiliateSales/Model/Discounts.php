<?php

namespace Product\AffiliateSales\Model;


use Magento\Framework\Model\AbstractModel;

/**
 *
 */
class Discounts extends AbstractModel
{
    protected $_eventPrefix = 'affiliated_discount';

    public function _construct()
    {
        $this->_init(\Product\AffiliateSales\Model\ResourceModel\Discounts::class);
    }
}
