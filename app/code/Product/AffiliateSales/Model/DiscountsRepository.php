<?php

namespace Product\AffiliateSales\Model;

use Product\AffiliateSales\Model\ResourceModel\Discounts\CollectionFactory;
use Magento\Framework\App\ResourceConnection;

class DiscountsRepository
{
    private $collectionFactory;
    private $resource;

    public function __construct(CollectionFactory $collectionFactory, ResourceConnection $resource)
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
    }

    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function getAffiliatedDiscount()
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_discount'); //gives table name with prefix

        $check = "SELECT title, offrate FROM $tableName";
        return $results1 = $connection->fetchAll($check);
    }

    public function getCommisionRate($title)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_discount'); //gives table name with prefix

        $commisionrate = "SELECT percentage, offrate FROM $tableName WHERE title = '$title'";
        return $results1 = $connection->fetchOne($commisionrate);
    }
}
