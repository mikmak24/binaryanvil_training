<?php

namespace Product\AffiliateSales\Model;

use Product\AffiliateSales\Api\ProductsRepositoryInterface;
use Product\AffiliateSales\Model\ResourceModel\Products\CollectionFactory;
use Magento\Framework\App\ResourceConnection;


class ProductsRepository implements ProductsRepositoryInterface
{
    private $collectionFactory;
    private $resource;

    public function __construct(CollectionFactory $collectionFactory,
                                ResourceConnection $resource)
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
    }

    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function setAffiliates($title, $affiliate_product_link, $product_sku)
    {
        $model = $this->collectionFactory->create();

        $model->getConnection()->insert('affiliate_products', ['title' => $title,'affiliate_product_link' => $affiliate_product_link
        , 'product_sku' => $product_sku]);
        $model->save();
    }

    public function setAffiliateSku($trackingCode, $sku, $selectedValue)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT trackingcode, productsku FROM $tableName WHERE trackingcode = '$trackingCode' AND productsku = '$sku'";
        $results1 = $connection->fetchOne($check);

        if(empty($results1))
        {
            $insert = "INSERT INTO $tableName (trackingcode, productsku, campaign) VALUES ('$trackingCode', '$sku', '$selectedValue')";
            $results = $connection->query($insert);
        }
    }


}
