<?php

namespace Product\AffiliateSales\Model\ResourceModel\Accounts;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Accounts;
use Product\AffiliateSales\Model\ResourceModel\Accounts as AccountsResource;

class Collection extends AbstractCollection
{
	protected $_idFieldName = 'id';

	protected function _construct()
	{
		$this->_init(Accounts::class, AccountsResource::class);
	}
}
