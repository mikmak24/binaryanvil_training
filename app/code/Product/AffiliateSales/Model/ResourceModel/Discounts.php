<?php

namespace Product\AffiliateSales\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Discounts extends AbstractDb
{
    public function _construct()
    {
        $this->_init('affiliated_discount', 'id');
    }
}
