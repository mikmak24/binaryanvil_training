<?php

namespace Product\AffiliateSales\Model\ResourceModel\Discounts;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Discounts;
use Product\AffiliateSales\Model\ResourceModel\Discounts as DiscountsResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(Discounts::class, DiscountsResource::class);
    }
}
