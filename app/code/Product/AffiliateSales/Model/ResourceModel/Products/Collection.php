<?php

namespace Product\AffiliateSales\Model\ResourceModel\Products;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Products;
use Product\AffiliateSales\Model\ResourceModel\Products as ProductsResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(Products::class, ProductsResource::class);
    }
}
