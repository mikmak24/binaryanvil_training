<?php

namespace Product\AffiliateSales\Model\ResourceModel\Sku;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Sku;
use Product\AffiliateSales\Model\ResourceModel\Sku as SkuResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(Sku::class, SkuResource::class);
    }
}
