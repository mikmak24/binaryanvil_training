<?php

namespace Product\AffiliateSales\Model;

use Magento\Framework\Model\AbstractModel;

/**
 *
 */
class Sku extends AbstractModel
{
    protected $_eventPrefix = 'affiliated_sku';

    public function _construct()
    {
        $this->_init(\Product\AffiliateSales\Model\ResourceModel\Sku::class);
    }
}
