<?php

namespace Product\AffiliateSales\Model;

use Product\AffiliateSales\Model\ResourceModel\Sku\CollectionFactory;
use Magento\Framework\App\ResourceConnection;

class SkuRepository
{
    private $collectionFactory;
    private $resource;

    public function __construct(CollectionFactory $collectionFactory, ResourceConnection $resource)
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
    }

    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function checkProductSku($sku)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT productSku FROM $tableName";
        $results1 = $connection->fetchAll($check);

        if(empty($results1))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function getTrackingCode($sku)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT trackingcode, campaign FROM $tableName WHERE productsku = '$sku'";
        return $list = $connection->fetchAll($check);
    }

    public function getCampaignType($sku, $trackingcode)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT campaign FROM $tableName WHERE productsku = '$sku' AND trackingcode = '$trackingcode'";
        return $campaigntype = $connection->fetchOne($check);
    }
}
