<?php

namespace Products\AffiliateSales\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Product\AffiliateSales\Model\SkuRepository;
use Product\AffiliateSales\Model\AccountsRepository;
use Product\AffiliateSales\Model\DiscountsRepository;
use Product\AffiliateSales\Model\ProductsRepository;

class CheckoutAccess implements ObserverInterface
{
    protected $_customerRepositoryInterface;
    protected $skurepo;
    protected $accountsrepo;
    protected $discountsrepo;
    protected $productsrepo;

    public function __construct(
        CustomerRepositoryInterface $customerRepositoryInterface,
        SkuRepository $skurepo,
        AccountsRepository $accountsrepo,
        DiscountsRepository $discountsrepo,
        ProductsRepository $productsrepo
    ) {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->skurepo = $skurepo;
        $this->accountsrepo = $accountsrepo;
        $this->discountsrepo = $discountsrepo;
        $this->productsrepo = $productsrepo;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllItems();
        foreach($items as $item)
        {
            $sku = $item->getSku();
            $price = $item->getPrice();
            $exist = $this->productsrepo->checkProductSku($sku);

            if($exist == true)
            {
                $listofdata = $this->productsrepo->getTrackingCode($sku);

                foreach($listofdata AS $list)
                {
                    $listofaffiliates = $this->accountsrepo->getTrackingCode();

                    foreach($listofaffiliates AS $listt)
                    {
                        if($list['trackingcode'] == $listt['trackingcode'])
                        {
                            $campaigntype = $this->productsrepo->getCampaignType($sku, $list['trackingcode']);
                            $commisionRate = $this->discountsrepo->getCommisionRate($campaigntype);

                            $this->accountsrepo->setCommisionRate($list['trackingcode'], $commisionRate, $price);
                        }
                    }

                }
            }
        }
    }
}