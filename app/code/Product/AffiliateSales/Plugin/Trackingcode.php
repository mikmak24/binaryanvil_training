<?php
namespace Product\AffiliateSales\Plugin;
use Magento\Customer\Controller\Account\CreatePost;
use Product\AffiliateSales\Model\AccountsRepository;
use Product\AffiliateSales\Block\Hello;
use Magento\Framework\App\Action\AbstractAction;


class Trackingcode
{
    protected $repo;
    protected $view;
    protected $request;


    public function __construct(AccountsRepository $repo,Hello $view,AbstractAction $request)
    {
        $this->repo = $repo;
        $this->view=$view;
        $this->request= $request;
    }

    function generateRandomCode($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = 'BAAF-';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function beforeExecute(CreatePost $createPost)
    {

        $fname = $this->request->getRequest()->getParam('firstname');
        $lname = $this->request->getRequest()->getParam('lastname');
        $email = $this->request->getRequest()->getParam('email');
        $pass = $this->generateRandomCode();
        $this->repo->setAffiliates($fname,$lname,$email,$pass);

    }
}