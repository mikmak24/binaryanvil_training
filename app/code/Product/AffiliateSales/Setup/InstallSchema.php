<?php

namespace Product\AffiliateSales\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
	/**
	* {@inheritdoc}
	*/

	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();

//		$table1 = $setup->getConnection()->newTable(
//			$setup->getTable('affiliate_accounts')
//		)->addColumn(
//			'id',
//			Table::TYPE_INTEGER,
//			null,
//			['identity' => true, 'nullable' => false, 'primary' => true],
//			'Item ID'
//		)->addColumn(
//			'firstname',
//			Table::TYPE_TEXT,
//			255,
//			['nullable' >= false],
//			'First Name'
//		)->addColumn(
//			'lastname',
//			Table::TYPE_TEXT,
//			255,
//			['nullable' >= false],
//			'Last Name'
//		)->addColumn(
//			'email_address',
//			Table::TYPE_TEXT,
//			255,
//			['nullable' >= false],
//			'Email Address'
//		)->addColumn(
//			'trackingcode',
//			Table::TYPE_TEXT,
//			255,
//			['nullable' >= false],
//			'Tracking Code'
//        )->addColumn(
//            'commisionrate',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Commision Rate'
//		)->setComment(
//			'Manage Affiliate Account'
//		);

//        $table2 = $setup->getConnection()->newTable(
//            $setup->getTable('affiliate_products')
//        )->addColumn(
//            'id',
//            Table::TYPE_INTEGER,
//            null,
//            ['identity' => true, 'nullable' => false, 'primary' => true],
//            'Product ID'
//        )->addColumn(
//            'title',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Title'
//        )->addColumn(
//            'affiliate_product_link',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Product Link'
//        )->addColumn(
//            'product_sku',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Product Sku'
//        )->setComment(
//            'Manage Affiliate Product'
//        );

//        $table3 = $setup->getConnection()->newTable(
//            $setup->getTable('affiliated_sku')
//        )->addColumn(
//            'id',
//            Table::TYPE_INTEGER,
//            null,
//            ['identity' => true, 'nullable' => false, 'primary' => true],
//            'Product ID'
//        )->addColumn(
//            'trackingcode',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Tracking Code'
//        )->addColumn(
//            'productsku',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Product Sku'
//        )->addColumn(
//            'campaign',
//            Table::TYPE_TEXT,
//            255,
//            ['nullable' >= false],
//            'Type of Campaign'
//        )->setComment(
//            'Manage Affiliate Product'
//        );

        $table4 = $setup->getConnection()->newTable(
            $setup->getTable('affiliated_discount')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Product ID'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Title'
        )->addColumn(
            'percentage',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Percentage'
        )->addColumn(
            'offrate',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'OffRate'
        )->setComment(
            'Manage Affiliate Product'
        );

        $setup->getConnection()->createTable($table4);
		$setup->endSetup();
	}
}
